using System;
using TechTalk.SpecFlow;

namespace APIProject.APISteps
{
    [Binding]
    public class UserManagmentStepDefinitions
    {
        [Given(@"User Enters user id (.*)")]
        public void GivenUserEntersUserId(int p0)
        {
            
        }

        [When(@"User send get user request")]
        public void WhenUserSendGetUserRequest()
        {
           
        }

        [Then(@"Validate user details (.*) (.*) and Http response code (.*)")]
        public void ThenValidateUserDetailsJanetWeaverAndHttpResponseCode(int p0)
        {
            
        }

        [Given(@"User eneters job (.*)")]
        public void GivenUserEnetersJobQA()
        {
            
        }

        [Given(@"User enters role (.*)")]
        public void GivenUserEntersRoleTEST()
        {
            
        }

        [When(@"User send create user request")]
        public void WhenUserSendCreateUserRequest()
        {
            
        }

        [Then(@"User should get created")]
        public void ThenUserShouldGetCreated()
        {
           
        }

        [Given(@"user want to update user id (.*)")]
        public void GivenUserWantToUpdateUserId(int p0)
        {
            
        }

        [When(@"User send update user request")]
        public void WhenUserSendUpdateUserRequest()
        {
            
        }

        [Then(@"User should get updated")]
        public void ThenUserShouldGetUpdated()
        {
            
        }

        [When(@"User send delete user request")]
        public void WhenUserSendDeleteUserRequest()
        {
            
        }

        [Then(@"Validate user is deleted (.*)")]
        public void ThenValidateUserIsDeleted(int p0)
        {
           
        }
    }
}

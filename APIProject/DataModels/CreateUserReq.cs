﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIProject.DataModels
{
    public class CreateUserReq
    {
        public string job { get; set; }
        public string name { get; set; }
    }
}

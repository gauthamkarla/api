﻿Feature: AddCustomer

Add customer using Admin portal

@smoke
Scenario Outline: Add Customer
Given User is on admin portal login page
When  User Enters valid credential <username> and <password>
And   User Clicks on Login button
When  User Navigates to Customer Dashboard page
And User Clicks on Add button
And User Enters <customerFirstName> and <customerLastName> and other mandatory fields
When User Clicks on Update Setting button
Then New Customer Should get Created and should display in Customers Management Page


Examples:
  | username  | password      | customerFirstName | customerLastName |
  | validUser | validPassword |      firstName    |      lastName         |
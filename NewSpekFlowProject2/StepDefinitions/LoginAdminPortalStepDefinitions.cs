using NewSpekFlowProject2.Hooks;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using TestProject1.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace NewSpekFlowProject2.StepDefinitions
{
    [Binding]
    public class LoginAdminPortalSteps
    {
        public IWebDriver driver; 
        int countBeforeAdding = 0;

        WebDriverClass webDriverClass = new WebDriverClass();

        [Given(@"User is on admin portal login page")]
        public void GivenUserIsOnAdminPortalLoginPage()
        {
            webDriverClass.OpenApplication();
            driver = BaseClass.MyDriver;
        }

        [When(@"User Enters valid credential (.*) and (.*)")]
        public void WhenUserEntersValid(String username, string password)
        {
            BaseClass baseClass = new BaseClass();
            LoginPage loginPage = new LoginPage(driver);
            baseClass.EnterText(loginPage.usernameTextField, TestContext.Parameters.Get("username"));
            baseClass.EnterText(loginPage.passwordTextField, TestContext.Parameters.Get("password"));
        }

        [When(@"User Enters invalid credential (.*) and (.*)")]
        public void WhenUserEntersInvalid(String username, string password)
        { 
            LoginPage loginPage = new LoginPage(driver);
            BaseClass baseClass = new BaseClass();
            baseClass.EnterText(loginPage.usernameTextField, TestContext.Parameters.Get("username"));
            baseClass.EnterText(loginPage.passwordTextField, TestContext.Parameters.Get("invalidPassword"));
        }

        [When(@"User Clicks on Login button")]
        public void WhenUserClicksOnLoginButton()
        {
            LoginPage loginPage = new LoginPage(driver);
            loginPage.loginButton.Click();
        }

        [Then(@"User should be logged in sussefully")]
        public void ThenUserShouldBeLoggedInSussefully()
        {
            DashboardPage dashBoardPage = new DashboardPage(driver);
            WebDriverWait wait = new WebDriverWait(driver,TimeSpan.FromSeconds(15));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[text()='Dashboard']")));
            Assert.AreEqual(true, dashBoardPage.customerText.Displayed);
            webDriverClass.CloseBrowser(driver);
        }

        [Then(@"User should get (.*)")]
        public void ThenUserShouldGetError(String errorMessage)
        {         
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//div[text()='Invalid Login Credentials']")));
            LoginPage loginPage = new LoginPage(driver);
            Assert.AreEqual(true, loginPage.invalidError.Displayed);
            webDriverClass.CloseBrowser(driver);
        }
        [When(@"User Navigates to Customer Dashboard page")]
        public void WhenUserNavigatesToCustomerDashboardPage()
        {
            DashboardPage dashBoardPage = new DashboardPage(driver);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(dashBoardPage.accountsLink));
            dashBoardPage.accountsLink.Click();   
            dashBoardPage.customersLink.Click();
        }

        [When(@"User Clicks on Add button")]
        public void WhenUserClicksOnAddButton()
        {
            CustomerManagementPage custManagementPage = new CustomerManagementPage(driver);
            countBeforeAdding = custManagementPage.customers.Count;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;

            for(; ; )
            {
                try
                {
                    custManagementPage.addButton.Click();
                    break;
                }
                catch(Exception e)
                {
                    jse.ExecuteScript("window.scrollTo(0, -100)");
                }
            }
            
        }

        [When(@"User Enters (.*) and (.*) and other mandatory fields")]
        public void WhenUserEntersCustomerFirstNameAndCustomerLastNameAndOtherMandatoryFields(String customerFirstName,String customerLastName)
        {
            AddCustomerPage addCustomerPage = new AddCustomerPage(driver);
            BaseClass baseClass = new BaseClass();
            baseClass.EnterText(addCustomerPage.firstNameTxtField, TestContext.Parameters.Get("customerFirstName"));
            baseClass.EnterText(addCustomerPage.lastNameTxtField, TestContext.Parameters.Get("customerLastName"));
            baseClass.EnterText(addCustomerPage.emailTxtField, baseClass.GenerateRandomNumber()+TestContext.Parameters.Get("customerEmail"));
            baseClass.EnterText(addCustomerPage.passwordTxtField, TestContext.Parameters.Get("customerPassword"));
            addCustomerPage.countryDropDown.Click();
            addCustomerPage.selectCountry.Click();
                      
        }

        [When(@"User Clicks on Update Setting button")]
        public void WhenUserClicksOnUpdateSettingButton()
        {
            AddCustomerPage addCustomerPage = new AddCustomerPage(driver);
            addCustomerPage.submitButton.Click();
        }

        [Then(@"New Customer Should get Created and should display in Customers Management Page")]
        public void ThenNewCustomerShouldGetCreatedAndShouldDisplayInCustomersManagementPage()
        {
            CustomerManagementPage custManagementPage = new CustomerManagementPage(driver);
            int countAfterAdding = custManagementPage.customers.Count;
            Assert.IsTrue(countAfterAdding > countBeforeAdding,"Customer creation failed");
            WebDriverClass webDriverClass = new WebDriverClass();
            webDriverClass.CloseBrowser(driver);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace NewSpekFlowProject2.Hooks
{
    internal class BaseClass
    {
        public static IWebDriver MyDriver;

        public void EnterText(IWebElement element, String text)
        {
            element.Clear();
            element.SendKeys(text);
        }

        public void SelectFromDropDownByText(IWebElement element,String text)
        {
            SelectFromDropDownByText(element, text);
        }

        public int GenerateRandomNumber()
        {
            Random rnd=new Random();
            return rnd.Next(100);
        }
    }
}

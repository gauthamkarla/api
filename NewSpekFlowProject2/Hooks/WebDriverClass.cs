﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewSpekFlowProject2.Hooks
{
    internal class WebDriverClass:BaseClass
    {
        public void OpenApplication()
        {
            String browser = TestContext.Parameters.Get("browser");

            if (browser.Contains("Chrome"))
            {
                MyDriver = new ChromeDriver();
                MyDriver.Manage().Window.Maximize();
                MyDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                MyDriver.Navigate().GoToUrl(TestContext.Parameters.Get("url"));

            }
            else if(browser.Contains("Firefox")){

            }
            else
            {

            }
        }

        public void CloseBrowser(IWebDriver driver)
        {
            driver.Quit();
        }
    }
}
